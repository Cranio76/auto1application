<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::domain('api.challenge.dev')->group(function() {

	// Login/logout routes

	Route::post('login', '\App\Api\V1\Controllers\LoginController@login')->name('api.login');
	Route::post('logout', '\App\Api\V1\Controllers\LoginController@logout')->name('api.logout');

	// Version routes

	Route::get('version', function() {return ['version' => '1']; })->name('api.version');

	Route::prefix('v1')->middleware('auth.api')->group(function() {

		// ME

		Route::get('me', '\App\Api\V1\Controllers\UserController@me');

		// Build character
		
		Route::post('character', '\App\Api\V1\Controllers\UserController@build');

		// PLAYING ROUTES GROUP - They all need an additional api.char middleware
		// because they need a character created
		
		Route::middleware('api.char')->group(function() {

			// Move
			Route::post('move', '\App\Api\V1\Controllers\CharacterController@move');
			// Look
			Route::get('look', '\App\Api\V1\Controllers\WorldController@look');
			// Pick
			Route::post('pick', '\App\Api\V1\Controllers\CharacterController@pick');
			
			Route::post('save', '\App\Api\V1\Controllers\GameController@save');
			
            Route::post('load', '\App\Api\V1\Controllers\GameController@load');
            
            Route::post('attack', '\App\Api\V1\Controllers\CharacterController@attack');

			// Example of RESTFul interface
			Route::resource('players', '\App\Api\V1\Controllers\UserController');
		});


	
	});


});