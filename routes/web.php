<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/start', 'StartController@pick')->name('start');

Route::get('/play', function() { 
	return view('play', [ 'size' => \App\Models\World\World::getWorldSize() ]); 
} )->name('play');

Route::get('/docs', function() {
	return 42;
})->name('docs');

