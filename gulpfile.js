
const gulp = require('gulp');
const md   = require('gulp-markdown');
const repl = require('gulp-replace');
const ins  = require('gulp-insert');

const renderer = new md.marked.Renderer();
renderer.code = function(code) {
    return 42;
}

const header = "<!DOCTYPE html> <html lang=\"en\"> <head> <meta charset=\"UTF-8\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"> <title>Document</title><link rel='stylesheet' href=\"app.css\" tyl></head> <body><div class='container'>";
const homelink = "<a href='README.html'>BACK TO INDEX</a>";
const footer = "</div></body></html>";


gulp.task('doc:home', function() {
    return gulp.src('./README.md')
        .pipe(md())
        .pipe(repl(/<code>\/docs\/(.*)\.md<\/code>/g, '<a href="$1.html">$1.html</a>'))
        .pipe(ins.wrap(header, footer))
        .pipe(gulp.dest('./docs/auto'))
});

gulp.task('doc:inner', function() {
    return gulp.src('./docs/*.md')
        .pipe(md())
        .pipe(repl(/<code>\/docs\/(.*)\.md<\/code>/g, '<a href="$1.html">$1.html</a>'))
        .pipe(ins.wrap(header + homelink, footer))
        .pipe(gulp.dest('./docs/auto'))
});

gulp.task('default', ['doc:home', 'doc:inner']);

