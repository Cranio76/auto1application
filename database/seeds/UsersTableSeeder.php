<?php

use Illuminate\Database\Seeder;

use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->truncate();
        DB::table('roles')->truncate();

        $role1 = new Role;
        $role1->name = 'master';
        $role1->save();

        $role2 = new Role;
        $role2->name = 'player';
        $role2->save();

        DB::table('users')->insert([
            'name' => 'Master',
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('waters'),
            'role_id' => $role1->id
        ]);
        DB::table('users')->insert([
            'name' => 'Player',
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('gilmour'),
            'role_id' => $role2->id
        ]);
    }
}
