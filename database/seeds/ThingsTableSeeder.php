<?php

use Illuminate\Database\Seeder;

use App\Factories\ThingFactory;

class ThingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(ThingFactory $factory)
    {

        DB::table('things')->truncate();
        
        $factory->create(2, 0, 'weapon', 'sword');
        $factory->create(3, 0, 'foe', 'sysadmin');
        $factory->create(2, 1, 'weapon', 'arrows');
        $factory->create(2, 2, 'wall');
        $factory->create(3, 1, 'foe', 'dracula');

    }
}
