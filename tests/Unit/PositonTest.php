<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\World\Position;

/**
 * A sample test case for unit tests
 */
class PositonTest extends TestCase
{
    public function test_it_creates_position()
    {
    	// Assest
        $position = new Position(4, 11);

        // Assert
        $this->assertEquals(4, $position->x);
        $this->assertEquals(11, $position->y);
    }

    public function test_it_negates_poisition()
    {
        // Assest
        $position1 = new Position(4, 11);

        // Act
        $position2 = $position1->neg();
    	
    	// Assert
    	$this->assertEquals([-4, -11], $position2->getCoords());	
    }

    public function test_it_adds_positions()
    {
    	// Assest
        $position1 = new Position(4, 11);
        $position2 = new Position(2, 3);

        // Act
        $position3 = $position1->add($position2);
    	
    	// Assert
    	$this->assertEquals([6, 14], $position3->getCoords());
    }

    public function test_it_subtracts_positions()
    {
    	// Assest
        $position1 = new Position(4, 11);
        $position2 = new Position(2, 3);

        // Act
        $position3 = $position1->sub($position2);
    	
    	// Assert
    	$this->assertEquals([2, 8], $position3->getCoords());
    }

    public function test_it_calculates_correct_modulo()
    {
    	// Assest
        $position = new Position(3, 4);

    	// Assert
    	$this->assertEquals(5, round($position->modulo(), 2));
    }

    public function test_it_calculates_correct_distance()
    {
    	// Assest
        $position1 = new Position(4, 5);
        $position2 = new Position(1, 1);

        // Act
        $distance = $position1->distance($position2);

    	// Assert
    	$this->assertEquals(5, $distance);
    }

    public function test_distance_is_order_invariant()
    {
    	// Assest
        $position1 = new Position(4, 5);
        $position2 = new Position(1, 1);

        // Act
        $distance1 = $position1->distance($position2);
        $distance2 = $position2->distance($position1);

    	// Assert
    	$this->assertEquals($distance1, $distance2);
    }    
}
