<?php
namespace Tests\Integration;

use Tests\TestCase;
use Tests\BaseTestTrait;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PlayingCharacterTest extends TestCase
{

    use RefreshDataBase;
    use BaseTestTrait;

    /**
     * Undocumented variable
     * @var \App\Factories\ThingFactory
     */
    public $thingFactory;

    public function setUp()
    {
        parent::setUp();
        $this->thingFactory = new \App\Factories\ThingFactory;
        $this->thingFactory->create(0, 0, 'weapon', 'fart');
    }
    
    public function test_picks_an_object()
    {
        // ASSEST
        $user = $this->assestUser();
        $character = $this->assestCharacter($user);

        // ACT
        $characterService = new \App\Services\CharacterService(
            new \App\Repositories\World\ThingRepository,
            new \App\Services\InventoryService
        );
        $characterService->pick($character);

        // ASSERT
        $this->assertCount(1, $character->getInventory());
    }
    
    public function test_picks_only_pickable_object()
    {
        // ASSEST
        $user = $this->assestUser();
        $character = $this->assestCharacter($user);
        $thing2 = $this->thingFactory->create(0, 0, 'laptop', 'asus');
        
        // ACT
        $characterService = new \App\Services\CharacterService(
            new \App\Repositories\World\ThingRepository,
            new \App\Services\InventoryService
        );
        $characterService->pick($character);

        // ASSERT
        $this->assertCount(1, $character->getInventory());
    }    

    public function test_picks_two_pickable_objects()
    {
        // ASSEST
        $user = $this->assestUser();
        $character = $this->assestCharacter($user);
        $this->thingFactory->create(0, 0, 'weapon', 'belch');

        // ACT
        $characterService = new \App\Services\CharacterService(
            new \App\Repositories\World\ThingRepository,
            new \App\Services\InventoryService
        );
        $characterService->pick($character);

        // ASSERT
        $this->assertCount(2, $character->getInventory());
    }

    public function test_picks_one_objects_of_two_in_different_positions()
    {
        // ASSEST
        $user = $this->assestUser();
        $character = $this->assestCharacter($user);
        $this->thingFactory->create(1, 0, 'weapon', 'belch');
        
        // ACT
        $characterService = new \App\Services\CharacterService(
            new \App\Repositories\World\ThingRepository,
            new \App\Services\InventoryService
        );
        $characterService->pick($character);

        // ASSERT
        $this->assertCount(1, $character->getInventory());
    }

    public function test_picks_two_objects_of_two_in_different_positions_moving()
    {
        // ASSEST
        $user = $this->assestUser();
        $character = $this->assestCharacter($user);
        $this->thingFactory->create(1, 0, 'weapon', 'belch');
        
        // ACT
        $characterService = new \App\Services\CharacterService(
            new \App\Repositories\World\ThingRepository,
            new \App\Services\InventoryService
        );
        $characterService->pick($character);
        $characterService->move($character, 'e');
        $characterService->pick($character);
        
        // ASSERT
        $this->assertCount(2, $character->getInventory());
    }    

}