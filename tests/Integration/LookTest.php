<?php

namespace Tests\Integration;

use Tests\TestCase;
use Tests\BaseTestTrait;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Services\CharacterService;
use App\Factories\ThingFactory;
use App\Repositories\World\ThingRepository;
use App\Services\InventoryService;

class LookTest extends TestCase
{

	use BaseTestTrait;
	use RefreshDatabase;

	function tests_it_looks_one_object_in_range()
	{
        $user = $this->assestUser();
		$character = $this->assestCharacter($user);
        $charSvc = new CharacterService(new ThingRepository, new InventoryService);
        $thingFac = new ThingFactory;
        $thing = $thingFac->create(1, 1, 'a', 'b');
        $things = $charSvc->look($character);
        $this->assertCount(1, $things);
    }
    
    function tests_it_looks_two_object_in_range()
	{
        $user = $this->assestUser();
		$character = $this->assestCharacter($user);
        $charSvc = new CharacterService(new ThingRepository, new InventoryService);
        $thingFac = new ThingFactory;
        $thing = $thingFac->create(1, 1, 'a', 'b');
        $thing2 = $thingFac->create(0, 1, 'a', 'b');
        $things = $charSvc->look($character);
        $this->assertCount(2, $things);
    }

    function tests_it_looks_only_object_in_range()
	{
        $user = $this->assestUser();
		$character = $this->assestCharacter($user);
        $charSvc = new CharacterService(new ThingRepository, new InventoryService);
        $thingFac = new ThingFactory;
        $thing = $thingFac->create(1, 1, 'a', 'b');
        $thing2 = $thingFac->create(2, 1, 'a', 'b');
        $things = $charSvc->look($character);
        $this->assertCount(1, $things);
    }    

    function tests_it_doesnt_look_objects_out_of_range()
	{
        $user = $this->assestUser();
		$character = $this->assestCharacter($user);
        $charSvc = new CharacterService(new ThingRepository, new InventoryService);
        $thingFac = new ThingFactory;
        $thing1 = $thingFac->create(2, 1, 'a', 'b');
        $thing2 = $thingFac->create(2, 2, 'a', 'b');
        $thing3 = $thingFac->create(3, 1, 'a', 'b');
        $thing4 = $thingFac->create(1, 2, 'a', 'b');
        $things = $charSvc->look($character);
        $this->assertCount(0, $things);
    }    
}