<?php

namespace Tests;

use App\Models\Role;
use App\User;

/**
 * This trait provides some common assesting helpers
 */
trait BaseTestTrait
{

    public function assestUser()
    {
        $role = new Role();
        $role->name = 'master';
        $role->save();

        $factory = new \App\Factories\UserFactory();
        return $factory->createByRoleName('John', 'doe', 'master', 'a@b.com');
    }

    public function assestCharacter(User $user)
    {
        $factory = new \App\Factories\CharacterFactory;
        return $factory->create("Beowulf", 
            new \App\Models\World\Position(0, 0),
            500, 
            100, 
            100,
            $user->getId()
        );
    }    

}