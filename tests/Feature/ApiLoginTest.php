<?php
namespace Tests\Feature;

use Tests\TestCase;
use Tests\BaseTestTrait;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Role;
use App\Factories\UserFactory;

class ApiLoginTest extends TestCase
{

    use RefreshDatabase;

    use BaseTestTrait;

    /**
     * General assest function. Does not use setUp() because we cannot
     * access the Models at that stage.
     * @return [type] [description]
     */
    public function setUp()
    {
        parent::setUp();
        $this->assestUser();
    }

    public function test_does_not_login_without_credentials()
    {
        // ASSEST

        // ACT
        $response = $this->json('POST', 'http://api.challenge.dev:8001/login', []);
        
        // ASSERT
        $response->assertStatus(401);
    }

    public function test_does_not_login_with_wrong_credentials()
    {
        // ASSEST

        // ACT
        $response = $this->json('POST', 'http://api.challenge.dev:8001/login', [
            'user' => 'John',
            'password' => 'deer'
        ]);
        
        // ASSERT
        $response->assertStatus(401);
    }

    public function test_does_login_with_correct_credentials()
    {
        // ASSEST

        // ACT
        $response = $this->json('POST', 'http://api.challenge.dev:8001/login', [
            'user' => 'John',
            'password' => 'doe'
        ]);

        // ASSERT
        $response->assertStatus(200);
    }

}