<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiGeneralTest extends TestCase
{
    use RefreshDatabase;

    public function test_does_reach_api()
    {
        $response = $this->get('http://api.challenge.dev/');
        $response->assertStatus(200);
    }

    public function test_does_reach_api_version()
    {
        $response = $this->get('http://api.challenge.dev/version/');
        $response->assertJson(['version' => 1]);
    }    
}