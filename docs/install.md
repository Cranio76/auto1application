# Installation

See `/docs/walkthrough.md`

## Requirements

* Docker
* Docker Compose
* npm
* MySQL5.6 
* PHP7.0
* A Unix-like shell environment

## Installation and running with Docker

The **Docker Compose** configuration is almost identical to the one provided
with the Symfony challenge. 

I made some **minor changes** here and there in order to:

* accomodate the different framework
* make Nginx respond to the specific API domain `api.challenge.dev`
* use the `.env` file provided in Laravel for a common environmente configuration
* **install a testing database** on another `mysqltest` container

### Install base files

1. Grab the archive
2. Unpack it and `cd` in the folder
3. Build Docker Compose with `docker-compose build`
4. Start Docker Compose with `docker-compose up -d`

### Edit hosts file
 
In the host machine, edit your `/etc/hosts` file by adding the following domains:

```
127.0.0.1    challenge.dev www.challenge.dev api.challenge.dev
```

### Install NodeJS packages

1. `cd` in the folder
2. `npm install`

### Install PHP packages

1. Log into the `php` machine with `docker-compose run -rm php bash`
2. Install PHP packages with `composer install`

### Prepare database

If there is already the database data under `/docker_data/mysql/` this step may 
not be necessary.

If not, or to **reset** the database to the demo data:

1. Log into the `php` machine with `docker-compose run -rm php bash`
2. Run `php artisan migrate` to prepare the schema
3. run `php artisan db:seed` to fill with data
4. Run `php artisan migrate --env=testing` to prepare the testing schema

### Test

Open the frontend UI by opening `http://www.challenge.dev:8001` on your 
browser (preferably Chrome).