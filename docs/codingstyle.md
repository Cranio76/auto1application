# Coding style

* I tried to be as **PSR-4 compliant** as possible (not 100% but always consistent)

* I have the habit to favor **long and meaningful class, variable, function names**
  to make code easier to understand without documentation

* I use **4 spaces indentation** (no tabs) and very less often microindentation
  (the latter only for special alignments)

* The beauty of PHP7.0 is **type hinting** also for primitive types, so I use
  it, on parameters and return types (no strict checks though here) :) 
  Laravel's IoC container service also uses type hinting for automatic **dependency 
  injection**, expecially in **controllers**.

## Casing

ONLY IN TESTS the methods are named with `snake_case` and not `camelCase`
because they are, in fact, very long sentences. In ANY other case, save for
direct DB field naming, I use **camelCase**.

## General architecture: MVC or?

I decided to follow a classical `MVC` pattern, although for a heavy action-based
API, an **Action-Domain-Responder** could be an interesting option to explore.

## Code Documentation

See `/docs/docs.md`.

## Exceptions

**Exceptions** are namespaced under `App/Exceptions` and derive from the common
PHP exception. They are few, so no furhter namespacing. For large application, I'd
subdivide them in further namespaces.

They are abundantly thrown in case of errors, wrong data, wrong actions, mainly
in **services**.

Most of the times, a **controller** would intercept any exception and return a
JSON error response via Http.

## Routes

See routes under `/routes/api.php` and `/routes/web.php`.

## Controllers

The controllers are ideally subdivided in:

* **web controllers**: meant for direct front-end usage (see `/app/Http/Controllers`)

* **api controllers**: entry points for API endpoints (see `/app/Api/V1/Controllers`)

Key points:

1. **All the controllers are lean**.  
   They don't do nothing more than elaborating the request, using the proper
   classes and services, and returning the response.

2. **They use dependency injection**.  
   I leverage Laravel's built IoC containers.

## Services

Almost all the business logic happens in the proper
**services** or anyway in specialized classes.

They are under `app/Http/Services`.

## Factories

Laravel has its own Model Factory facilitations and
helpers; I'd just show siple, made-from-scratch
factories used to instantiate some objects.

They are under `app/Http/Factories`.

## Repositories

There is some examples on how I'd implement Repositories to add
a useful layer between the data tier and the business logic.
This makes context swapping 

They are under `app/Http/Repositories`.