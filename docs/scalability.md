# Scalability

As conceived in such a short timespan, there is no
coherent large-scale design which would anyway
take in consideration a much larger product vision.

I'd rather point out what I **would** implement for a
large scale RPG:

* any **asynchronous task with no real-time urgency** 
  like sending emails or making statistics would be delegated 
  to **queues** and **queue workers**. Laravel has built-in 
  support for many queue drivers (such as Redis)

* even better, the use of a **distributed message broker** 
  like **Apache Kafka** for it's replicabilty and resilience
  to fault tolerance (I am using it in my last project to 
  communicate between PHP and NodeJS services and really like 
  its philosophy)

* I would delegate specific tasks to **microservices** which
  interoperate through **REST interfaces** (maybe leveraging
  the same routes). For example, authentication could be managed
  by a separate service (in any language, say **NodeJS**)

* To prepare the code for microservice scalability, I'd use my
  own **PHP services and repositories** to make possible 
  **fast service swapping** with **minimum or no code refactoring**.

* For the same reason, I like very much **developing with Docker
  and Docker Compose**, to replicate on my local machine the
  production environment together with the use of **.env environment
  files** and **environmenet-specific tasks**

* The **codebase maintanability** would be done with **GIT** and 
  **GIT submodules** to have better concern separation

* Here we use MySQL directly. I would leverage **caching** and most
  of all **in memory caching** (REDIS) whenever possible for very
  common requests. I would use **other storage types** like **Elasticsearch**
  or **SOLR** for fast searches and aggregations.

* As an example, the character's **inventory** uses the `Cache`
  façade which is filesystem based but can be effortlessly switched
  to **REDIS** for other uses