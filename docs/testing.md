# Testing

All the tests are written for **PHPUnit**, but enriched with **Laravel**'s
approach to BDD for Feature tests.

## Docker configuration

There is a **database double** for testing which is intended not to be
seeded. I've prepared an additional **Docker image** for testing purposes.

## Get PHPUnit

**PHPUnit should be available from the container** as a **command**, as I have
included the necessary Docker configuration.

If you don't have `phpunit` installed as a command or the PHAR
by hand, you can install the PHAR.

First, `cd` into the **root folder**.
Then, download **PHPUnit** into the **root folder**:

```
wget https://phar.phpunit.de/phpunit.phar
```

## Configuration

There is a `phpunit.xml` file with a, I must say, very standard
configuration.

## Launch test suite

1. Log into php container with `docker-compose run --rm php bash`
2. Launch tests with `phpunit` or `php phpunit.phar`

## Test writing approach

### Where to find tests

All the tests are under the `/tests` folder, subdivided in the pretty
self-explanatory `Unit`, `Integration` and `Feature`.

### Implementation philosophy

Generally speaking, I try to code following the **TDD katas** philosophy
with **test first, code then**.

When deadlines are important, I make sure to have a very good **code coverage**
ideally for all, but expecially for the most sensitive parts.

In this case, having not much time, I did a few example test to show my skills.

### Coding philosophy

I generally divide the tests in three parts: 

1. **Assest** (prepare all the needed objects and so on)
2. **Act** (do something, trigger side-effects, calculations ...)
3. **Assert** (check the result with assertions)

I also often prepare a **base PHP trait** which contains some common
initializations, object assesting, etc.
(Here I use `/tests/BaseTestTrait.php`).

