<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

use App\Repositories\World\ThingRepository;
use App\Services\CharacterService;
use App\Services\UserService;

use App\Api\V1\Transformers\ThingTransformer;

/**
 * The World endpoints represent interactions with the surrounding world
 */
class WorldController
{

	/**
	 * Thing repository
	 * @var ThingRepository
	 */
	private $thingRepository;

	/**
	 * Character service
	 * @var [type]
	 */
	private $characterService;

	/**
	 * UserService
	 * @var UserService
	 */
	private $userService;

    /**
     * Constructor
     * @param ThingRepository $thingRepository
     * @param UserService $userService
     * @param CharacterService $characterService
     */
	public function __construct(
		ThingRepository $thingRepository,
		UserService $userService,
		CharacterService $characterService)
	{
		$this->thingRepository = $thingRepository;
		$this->userService = $userService;
		$this->characterService = $characterService;
	}

	/**
	 * Look around and return object withing the character's
	 * gaze range
	 * @api
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function look(Request $request)
	{
		$character = $this->userService->getCurrentCharacter();
		$gazeRange = $character->getGaze();
		$position = $character->getPosition();
		return ThingTransformer::transformCollection(
			$this->thingRepository
				 ->getInSquareRange($position, $gazeRange)
				 ->get()
		);

	}

}
