# Frontend

I've made a simple frontend with an API visor.

The frontend is organized in **modules** and it is built
with `npm`, after being packed by **Webpack**.

**The frontend JS coding is rudimentary as I had no time to 
make elegant code, proper JS encapsulation and interfaces,
and so on.**

It uses **axios** as a client-side Http client.

