# Walkthrough

Ok guys, let me walk you through the implementation.

## From the frontend

Go to `http://www.challenge.dev:8001/play`.

Enjoy the little console I've prepared for you.

## From the CLI

```
# Log in as Master user and grab the token with:
TOKEN=$(curl -s http://api.challenge.dev:8001/login -X POST -d "user=Master&password=waters&plaintext=true")
THEAD="Authorization: Bearer $TOKEN"

# Who are you
curl -H "$THEAD" -s http://api.challenge.dev:8001/me

# Create a character for your user
curl -H "$THEAD" -s http://api.challenge.dev:8001/v1/build -d "name=Atreyu"

# Have a look at the surrounding world
curl -H "$THEAD" -s http://api.challenge.dev:8001/v1/look"

# Logout
curl -H "$THEAD" -X POST -s http://api.challenge.dev:8001/logout 
```
