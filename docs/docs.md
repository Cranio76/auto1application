# Documentation

There is the automatically generated **API class documentation** and
the **project instructions**.

Both are linked from the homepage at `http://www.challenge.dev:8001`.

## Code documentation

Most classes, interfaces, method, properties, functions or traits are
thoroughly documented in the classic **PHPDocumentor** style.

### Automatic generation

A `phpdoc.dist.xml` file provides a standard configuration for **PHPDoc
generation** with the possibility to specify a local `phpdoc.xml`.

A HTML output of the documentation can be found under `/app/docs/api`, just
open `/docs/html/index.html` in any browser.

> As a general rule, my documentation tends to be rich, crystal clear, full of
> cross-references, both textual and with PHPDoc references like `@see`.
> So it's easier for other programmers (and for the forgetful-future-me <smile>)
> to understand what's going on.

## Manuals

The textual documentation (instructions, manuals, ...) are written in 
**markdown** and are subdivided between the `README.md` file and the `*.md`
files under `/docs/`.

Open `README.html` in your favourite browser.

### Automatic generation

A little **Gulp task** in `gulpfile.js` manages to take all the Markdown
docs (like this one) and automatically transform it into fully formatted 
HTML in the folder `/docs/auto`.

Just type `gulp` from the root directory of the project.

