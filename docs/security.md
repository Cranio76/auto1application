# Security

## Secret key

A **secret key** as salt for the whole framework has been set.

## API security

The API uses **JWT** for authentication with (this)[https://github.com/tymondesigns/jwt-auth/wiki] package.

JWT Tokens are coupled with Laravel's own Users and Auth class.

The tokens have an expiration time and can ideally be invalidated also specifically to reduce the attack time window for a compromised token.

## Other security issues

Not very much.

Data has **validation rules** (at least, where I had the chance to implement
them).

All queries pass through the ORM (Eloquent) so there is no possibility for 
any SQL injection.

There is no **CSRF token** verification, but only because there are no client-side
forms implemented in this MVP.

