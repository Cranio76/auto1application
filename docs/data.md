# Data

An overview of how the data is organized

## ORM: rationale and approach

We make use of Laravel's **Eloquent** ORM. While it has
a very beautiful fluent syntax, it has also some
shortcomings.

The main one is that it uses **reflection** and **magic
methods** with some oddities: the worst is that *it doesn't
throw an exception when calling a non-existent attribute.

So, from the experience on my previous work on **Symfony**
and **Doctrine**, I decided **not to use any direct 
property access on the model class**.

Instead, I delegated any communication with the model
to **getters** and **setters** and performed most CRUD
operations with **repositories**.

The models are under `app/Models` except for `app/User.php`
(it's a common Laravel convention to have User there).

## User

Model: `App\User`

The name says it all. :) 

## The world

Model: `App\Models\World\Thing`

Fields: `id`, `x`, `y`, `what`, `type`

* the world is defined by a **series of square tiles**
* any tile has **x** and **y** coords
* `Thing` places objects in that matrix, effectively
  representing a **sparse matrix of objects**
* Why? This makes possible to have more than one object
  in a square, for any extensible use of this property.
  Maybe there can be both a hero and a weapon, so the
  hero can decidet to grab it or not.
* `what` represents the type of object occupying coords
  x and y. 
  This is intended to be a **polymorphic relation**
  where we can obtain a specific class by **strategy
  pattern** according to this field.
* `type` is a subtype for `what`