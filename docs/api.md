# API

The API code is located under `/app/Api/V1/`.
As you can see it features **API versioning**.

I've avoided using pre-made API libraries like (Dingo)[https://github.com/dingo/api/]
and preferred to show something made (almost) from scratch.

For the sake of showing some coding, I decided also not to leverage the prebuild
Laravel Guards.

## Authorization

The API uses **JWT tokens**, which are subsequently checked by checking the standard 
`Authorization: Bearer {TOKEN}` HTTP Header. I had no time to implement a **token refresh**
mechanism but I'm aware of it's implementation details. (The token anyway can be invalidated
specifically by logout and expires after a while).

## Routing

Have a look at the routes and the endpoint structure in `/routes/api.php`.

## Calling style and versioning

The API answers on the `api.challenge.dev` domain, with a generic endpoint structure
of `http://api.challenge.dev/{version}/{endpoint}`.

The only version available in this example is `v1`.

As a good practice, a **version endpoint** is included with a `GET` request to 
`http://api.challenge.dev/version/`, to show the current API version.

See below for **API reference**.

## REST

**TBH, there is not much RESTful approach in the code I've prepared**.

Why? because the pseudo-game is more centered on *actions* to perform with 
feedback than on *CRUD operations*.

Nonetheless, **to prove that I am familiar with RESTful endpoints** and how they
must be treated, an interface for creating new players 

## HTTP Responses

Every API response is in JSON and has the proper status code (e.g. `401` for
unauthorized access).

### Errors

In the case of **errors**, the response will be a JSON like 
`{error: "someErrorAlias"}` with a proper **HTTP status** (401, 404...)
The idea is that `someErrorAlias` is a key that will be fed to a **l10n/i18n** 
engine.

## Middleware

Under `app/Http/Middleware` there are a few middleware I use on the API 
requests, mostly as **access guards**: 

* `CustomApiAuth` checks if the user is authorized (via JWT)
* `MustHaveCharacterCreated` checks if the user has an own playing character
  (otherwise no game is possible)

## Metadata

I've used a classic approach of formatting the responses in **JSON format** 
under a `data` attribute. When suitable, also pagination is included:

```
{
    'data': {
        // actual data
    },
    'meta': {
        'paged' => true,
        'total' => 45,
        'page' => 1,
        'prev' => null,
        'next' => 'api/endpoint/?page=2'
    }
}
```


## Transformers a.k.a. Serializers

I made some **serializers** (known elsewhere also as **transformers**), classes 
which specialize in transforming all relevant data (models and so on) in 
consumable JSON objects for the API, with the intent of:

* hiding irrelevant or sensitive information to the API users
* formatting the data in an usable form from the client-side
* providing different representations of the same data using a different Transformer

This implements in a *very rudimentary way* the things that full-fledged 
libraries like (Fractal)[https://github.com/thephpleague/fractal] do.

Under `app/Api/V1/Transformers` there is an `AbstractTransformer` providing base
functionality (a blueprint for a basic `transform()` action and implementation
to transform collections with `transformCollection()`.

## Controllers

All the API-specific controllers are located under `app/Api/V1/Controllers`. 
They are derived from Laravel's own controller with some added functionality.

> All the controllers are as lean as possible. Any functionality 
> is deferred to the **services** and other classes. 

## Services

All the custom services are under `app/Http/Services`.

> The services do not generate any response of any kind. Everything
> is managed thru **Exceptions** and captured elsewhere (i.e. controllers).


# API reference

## Login

`POST http://api.challenge.dev:8001/login/`

Params `user`, `password`

Logs a user in. Returns a response with the **JWT token** like `{"token": "JWT_TOKEN"}`

This token must be used in **all the subsequent API calls** by including it in the
HTTP requests with a header like: `Authorization: Bearer JWT_TOKEN`

## Logout

`POST http://api.challenge.dev:8001/logout`

Logs the user out.

## Build character

`POST http://api.challenge.dev:8001/character`

Params `name`

Creates a character to be associated with the player

## Me

`GET http://api.challenge.dev:8001/v1/me`

A description of me (current user & character).

## Move

`POST http://api.challenge.dev:8001/move`

Params `direction`

Moves in a direction (`"e"`, `"w"`, `"sw"` ...)

## Pick

`POST http://api.challenge.dev:8001/pick`

Picks the objects in the current tile

## Look

`GET http://api.challenge.dev:8001/look`

Gets an overview of the neighboring squares in a distance
equal to the character's `gaze`.

**To be implemented:** The distance is calculated within
a square of `2*gaze+1` size, there is a stub for a 
circular distance but not implemented.
