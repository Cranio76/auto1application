# Da game

You control a **hero** that moves in **LocalhostLand**, whose
purpose is to **wander aimlessly in a supertiny world** to obtain
the ultimate goal of **making me hire by your company**.

## Movement

In the pursuit of this holy quest, you move in a world by moving
1 step at time in any of the 4 cardinal directions (N, S, W, E).

## Foes

It is rumored that you might encounter **some enemies** along the way.
Ancient tales tell of a fabled **sysadmin** hiding somewhere in the 
east, and a much more **evil creature** that inhabits the
territories to the south.

## Objects

LocalhostLand is not so fancy. The only thing you might find along the
way are some **walls** and **weapons**. 

Nope, no money.

## Weapons

They have a **name** and a **damage score**. That's it :) 

Just kidding, there is also a **range** that makes some
long-range attacks possible.

## Fights

Should you encounter some strange creature with a not-so-friendly
attitude, maybe the best advisable diplomacy stunt is to slay it
down without mercy.

Any creature (yes, **you** included!) has a **stamina**, an **attack**
force and a **defend** force.

### Battle formula

**I know** that this formula is dumb, dumber aned more dumb
than the dumbest thing in the world. 

The damage on the stamina is calculated this way:

```
D = W * A / 100
```

where:

* **D** is the actual damage of the blow
* **W** is the damage potential of the weapon
* **A** is the attack coefficient of the attacker

The **defend coefficient** is more or less the probability
that the blow is actually landed.

So, a high defend coefficient would make a weak character
able to fend off or dodge some blows, but would suffer anyway
massive blows from very powerful weapons and enemies.
