<!DOCTYPE html>
<html>
<head>
	<title>
		
	</title>
	<link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
    <div id="app">
     

	<div class="rpg-app">

		<div class="row">
			<div class="col-md-6">
				<h4>API Toolbox</h4>

				<button onclick="api.version()">API version</button>
				<button onclick="api.login()">No Credentials Login</button>
				<button onclick="api.login('Master', 'foo')">Bad Credentials Login </button>
				<button onclick="api.login('Master', 'waters')">Legit Master Login </button>
				<button onclick="api.login('Player', 'gilmour')">Legit Player Login </button>
				<button onclick="api.logout()">Logout</button>
				<hr>
				<button onclick="api.build('Joe Dev')">Build Character</button>
                <span v-if='me'>
                    Player: <strong>@{{me.name}}</strong>
                    Character name: <strong v-if='me.character'>@{{ me.character.name }}</strong>
                    <br>
                    Stamina: <strong v-if='me.character'>@{{ me.character.stamina }}</strong>
                    Attack: <strong v-if='me.character'>@{{ me.character.attack }}</strong>
                    Defense: <strong v-if='me.character'>@{{ me.character.defense }}</strong>
                    <br>
                    Gaze: <strong v-if='me.character'>@{{ me.character.gaze }}</strong>
                    Pace: <strong v-if='me.character'>@{{ me.character.pace }}</strong>
                    Pos: <strong v-if='me.character'> @{{ me.character.position }}</strong>
                </span>
                <hr>
			</div>
			<div class="col-md-6">
				<h4>Actions</h4>
				<button onclick="api.me()">Me</button>
				<button onclick="api.move('n')">N</button>
				<button onclick="api.move('s')">S</button>
				<button onclick="api.move('e')">E</button>
				<button onclick="api.move('w')">W</button>
				<button onclick="api.look()">Look</button>
				<button onclick="api.pick()">Pick</button>
				<button onclick="api.load()">Load game</button>
				<button onclick="api.save()">Save game</button>
				<br>
				<strong>Token: </strong> <span id="tokenspan"></span>
                <br>
                <h4>What do I see around me</h4>

                <span style='margin: 2px;' class='label label-warning' v-for='item in look'>
                    @{{ item.what }} at [@{{ item.position.x}},@{{ item.position.y}}]
                </span>

			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<strong>RESTful interface example (must be logged as Master):</strong>
				<br>
				<button onclick='api.index()'>Show Players</button>
				<button onclick='api.show()'>Show Player</button>
				<button onclick='api.create()'>Create Player</button>
				<button onclick='api.update()'>Update Player</button>
				<button onclick='api.destroy()'>Delete Player</button>
			</div>
			<div class="col-md-6">
            	
                <strong>Inventory: </strong>
                <template v-if='me.character'>
	            	<span class='label label-info' v-for='item in me.character.inventory'>
	            		@{{item.what}}/@{{item.type}}
	            	</span>
                </template>
                <br>

				<strong>What's here: </strong> <span id="whatshere"></span>
				<br>
				<strong>Message: </strong> <span id="msg"></span>
Message: "I am Master"
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="request-monitor">
					<pre class='code' id="visor" style='height:200px;overflow-y: scroll'></pre>
				</div>
			</div>
			<div class="col-md-6">
                <div style='display:table'>
                    <div style='display:table-row' 
                        v-if='me.character'
                        v-for='y in [0,1,2,3]'>
                        <a
                            style='display:table-cell' 
                            class='cell'
                            v-bind:class='{me: 
                                x==me.character.position[0] 
                                && y==me.character.position[1]
                            }'
                            v-on:click='api.attack(x,y)'
                            v-for='x in [0,1,2,3]'>
                            @{{ content[x][y] || '-' }}
                        </a>
                    </div>
                </div>
			</div>
		</div>
	</div>
    </div>
	<script type="text/javascript" src="/js/app.js"></script>

</body>
</html>	