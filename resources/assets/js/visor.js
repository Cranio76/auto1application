module.exports = {

	addLine: function(line) {
		document.getElementById("visor").innerHTML += line.trim() + "\n";
		this.trim(30);
	},

	addLines: function(lines) {
		const self = this;
		lines.push("");
		lines.forEach(function (line) {
			self.addLine(line.toString());
		})
	},

	trim: function(numberOfLines) {
		let inner = document.getElementById("visor").innerHTML.split("\n");
		let lastNLines = inner.slice(-numberOfLines);
        let trimmed = lastNLines.join('\n');
        const visorObj = document.getElementById("visor"); 
        visor.innerHTML = trimmed;
        visor.scrollTop = visor.scrollHeight;
	}

}