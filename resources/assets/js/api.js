const visor = require('./visor');
const ui = require('./ui');

let me;

module.exports = {


    formatResponse: function(res) {
        if (!res) {
            return [];
        }
        const method = res.config.method.toUpperCase();
        const completeCall = method + " " + res.config.url;
        const configData = res.config.data || '';
        const response = res.data ? JSON.stringify(res.data) : '';
        return [completeCall, "DATA " + configData, response];
    },

    showResponseText: function(res) {
        const lines = this.formatResponse(res);
        visor.addLines(lines);
        ui.drawBoard(me);
    },

    showErrorText: function(error) {
        console.log(error);
        const lines = this.formatResponse(error.response);
        lines.push("ERROR");
        visor.addLines(lines);
    },

    _get: function (url) {
        return instance.get(url)
            .then(res => this.showResponseText(res))
            .catch(err => this.showErrorText(err));
    },

    _post: function (url, data) {
        return instance.post(url, data)
            .then(res => this.showResponseText(res))
            .catch(err => this.showErrorText(err));
    },

    version: function() {
        return this._get('version');
    },

    setToken: function(token) {
        console.log("Setting token to " + token);
        window.authToken = token;
        ui.setToken(token);
        instance.defaults.headers['Authorization'] = "Bearer " + token;
    },

    login: function(user, password) {
        instance.post('login', {user: user, password: password})
            .then(response => {
                this.showResponseText(response);
                if (response.data.token) {
                    this.setToken(response.data.token);
                    this.me();
                }
            })
            .catch(err => {
                this.showErrorText(err);
                if (err.response.data.error == 'missing_credentials') {
                    ui.setMsg('Missing credentials.');
                }
                if (err.response.data.error == 'unauthorized') {
                    ui.setMsg('Login error: unauthorized access.');
                }                
            });
    },

    logout: function() {
        instance.post('logout')
            .then(res => {
                this.showResponseText(res);
                this.setToken('');
            })
            .catch(err => this.showErrorText(err));
    },

    me: function() {
        return instance.get('v1/me')
            .then(res => {
                ui.setMsg("I am " + res.data.name);
                me = res.data;
                this.look();
                this.showResponseText(res);
                app.me = res.data;
                if (app.me.character) {
                    app.inventory = app.me.character.inventory;
                }
            })
            .catch(err => this.showErrorText(err));
    },

    move: function(direction) {
        ui.setMsg('');
        return instance.post('v1/move', {direction: direction })
            .then(res => {
                this.showResponseText(res);
                if (res.data.whats_here) {
                    ui.setHere(res.data.whats_here);
                }
                this.me();
            })
            .catch(err => {
                if (err.response.data) {

                    if (err.response.data.error == 'tile_occupied') {
                        let message = "Can't move in direction " 
                            + direction 
                            + "! Tile is occupied by "
                            + err.response.data.what.map(function(i) { 
                                return i.what + " of type " + (i.type || i.what); 
                            }).join(" and ");
                        ui.setMsg(message);
                    }

                    if (err.response.data.error == 'out_of_bounds') {
                        let message = "Can't move in direction " 
                            + direction 
                            + "! The world ends here :(";
                        ui.setMsg(message);
                    }
                }
                this.showErrorText(err);
            });
    },

    build: function(name) {
        return instance.post('v1/character', {name: name})
            .then(res => this.showResponseText(res))
            .then(res => this.me())
            .catch(err => {
                this.showErrorText(err);
                const error = err.response.data.error;
                if (error == 'token_invalid') {
                    ui.setMsg('Cannot build character. Maybe you have to login?')
                }
            });
    },

    clear: function() {
        app.content = [
            [' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' '],
        ];
    },

    look: function() {
        return instance.get('v1/look')
            .then(
                res => {
                    this.showResponseText(res);
                    app.look = res.data;
                    this.clear();
                    if (res.data.length > 0) {
                        for (let i of res.data) {
                            let p = i.position;
                            app.content[p.x][p.y] = i.what + "\n" + i.type;
                        }
                    }
                }
            )
            .catch(
                err => this.showErrorText(err)
            );
    },

    pick: function() {
        return instance.post('v1/pick')
            .then(res => {
                this.showResponseText(res);
                this.clear();
                app.me.character.inventory = res.data;
                this.look();
            })
            .catch(err => this.showErrorText(error));
    },
 
    save: function() {
        const self = this;
        return instance.post('v1/save')
            .then(res => this.showResponseText(res))
            .catch(err => this.showErrorText(err));
    },

    load: function() {
        const self = this;
        return instance.post('v1/load')
            .then(res => this.showResponseText(res))
            .catch(err => this.showErrorText(err));                  
    },

    index: function() {
        return instance.get('v1/players')
            .then(res => this.showResponseText(res))
            .catch(error => this.showErrorText(error));
    },

    show: function() {
        return instance.get('v1/players/2')
            .then(res => this.showResponseText(res))
            .catch(error => this.showErrorText(error));
    },

    update: function() {
        return instance.put('v1/players/2', {
                name: 'Bill'
            })
            .then(res => this.showResponseText(res))
            .catch(error => this.showErrorText(error));
    },

    create: function() {
        return instance.post('v1/players/', {
                name: 'Dude',
                password: 'hey',
                role: 'master',
                email: Math.ceil(Math.random()*10000) + '@d.com'
            })
            .then(res => this.showResponseText(res))
            .catch(error => this.showErrorText(error));
    },

    destroy: function() {
        return instance.delete('v1/players/2')
            .then(res => this.showResponseText(res))
            .catch(error => this.showErrorText(error));
    },

    attack: function(x, y) {
        return instance.post('v1/attack', { x: x, y: y })
            .then(
                res => {
                    this.showResponseText(res);
                    this.clear();
                    this.look();
                })
            .catch(error => this.showErrorText(error));
    }



}