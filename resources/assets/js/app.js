
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

window.authToken = "NO TOKEN";

Vue.component('example', require('./components/ExampleComponent.vue'));
Vue.component('inventory', require('./components/Inventory.vue'), {
    props: ['items']
});



console.log(app);

const axios = require('axios');

window.instance= axios.create({
  baseURL: 'http://api.challenge.dev:8001/',
  timeout: 1000,
  headers: {'Authorization': 'Bearer I_AM_FAKE'}
});

window.api = require('./api');

window.app = new Vue({
    el: '#app',
    data: {
        me: '',
        inventory: [],
        look: [],
        api: api,
        content: [
            [' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' '],
        ],
    }
});