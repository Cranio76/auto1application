module.exports = {

	setToken: function(token) {
		document.getElementById('tokenspan').innerHTML = token.substr(0, 10) + "...";
	},

	setMe: function(me) {
		document.getElementById('me').innerHTML = JSON.stringify(me);
	},

	setHere: function(what) {
		document.getElementById('whatshere').innerHTML = JSON.stringify(what);
	},

	setMsg: function(what) {
		document.getElementById('msg').innerHTML = JSON.stringify(what);
    },

    setInventory: function(what) {},
    
	getCellObj: function(x, y) {
		return document.getElementById('b'+x+y);
	},

	setCell: function(x, y, content) {
	},

	clearBoard: function() {
	},

	drawBoard: function(me) {
	}

}