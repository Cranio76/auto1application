<?php 

namespace App\Api\V1\Controllers;

use App\Services\GameService;

use Illuminate\Http\Request;

/**
 * Game load/save controller.
 * @see App\Services\GameService
 */
class GameController
{

	private $gameService;

	public function __construct(GameService $gameService)
	{
		$this->gameService = $gameService;
	}

	public function save(Request $request)
	{
		$user = $request->user();
		$game = $this->gameService->save($user);

		return $game;
	}

	public function load(Request $request)
	{
		$user = $request->user();
		$this->gameService->load($user);
	}

}