<?php

namespace App\Api\V1\Controllers;

use Auth;

use App\Factories\CharacterFactory;
use App\Models\World\Position;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

use App\Api\V1\Transformers\UserTransformer;
use App\Services\UserService;
use App\Factories\UserFactory;
use App\Repositories\UserRepository;

use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * User controller
 */
class UserController extends Controller implements IRestful
{

	/**
	 * User factory
	 * @var UserFactory
	 */
    private $userFactory;
    
    /**
     * User repository
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Constructor
     * @param UserFactory $userFactory
     * @param UserRepository $userRepository
     */
	public function __construct(UserFactory $userFactory, UserRepository $userRepository)
	{
		$this->userFactory = $userFactory;
		$this->userRepository = $userRepository;
	}

	/**
     * Information about the logged in user and the character he is playing.
	 * @api
	 * @return [type] [description]
	 */
	public function me(UserService $userService) 
	{
		return UserTransformer::transform($userService->getCurrentUser());
	}

	/**
	 * Build the player's character and associate it
	 * @api
	 * @param  Request          $request 
	 * @param  CharacterFactory $factory 
	 */
	public function build(Request $request, CharacterFactory $factory, UserService $userService)
	{
        $user = $request->user();
		$character = $userService->getCharacter($user);

		$validator = Validator::make($request->all(), [
		    'name' => 'required'
		]);

		if ($validator->fails())
		{
			return response()->json($validator->errors(), 401);
		}

		// Checks if the character is already created
		if ($character)
		{
			return response()->json(['error' => 'already_created'], 403);
		}

		$factory->create($request->input('name'), new Position(0, 0), 500, 100, 100, $user->id);

		return response()->json(['ok'], 200);
	}

	/**
	 * RESTFUL REQUEST
	 */

    /**
     * Returns true if the user has "master" role
     * @see \App\User
     * @see \App\Models\Role   
     * @param Request $request
     * @return boolean
     */
	private function isMasterUser(Request $request)
	{
		$user = $request->user();
		$roleName = $user->getRoleName();
		return $roleName == 'master';
    }
    
    /**
     * Obtain standard user validator
     * @param Request $request
     * @return Validator
     */
	private function getValidator(Request $request)
	{
		return Validator::make($request->all(), [
		    	'name' => 'required',
		    	'email' => 'email|required',
		    	'password' => 'required',
		    	'role' => 'required',
            ]);
	}

    /**
     * REST index
     * @api
     * @param Request $request
     * @return void
     */
	public function index(Request $request)
	{
		if ($this->isMasterUser($request)) {
			return UserTransformer::transformCollection($this->userRepository->index());
		} else {
			return response()->json(['error' => 'unauthorized', 401]);
		}
	}

	/**
	 * REST show
	 * @api
	 * @param  Request $request [description]
	 * @param  [type]  $id      [description]
	 * @return [type]           [description]
	 */
	public function show(Request $request, $id)
	{
		if ($this->isMasterUser($request)) {
			try {
				return UserTransformer::transform($this->userRepository->find($id));
			} catch (ModelNotFoundException $e) {
				return response()->json(['error' => 'not_found', 404]);
			}
		} else {
			return response()->json(['error' => 'unauthorized', 401]);
		}		
	}

    /**
     * REST update
     * @api
     * @param Request $request
     * @param int $id
     * @return void
     */
	public function update(Request $request, $id)
	{
		if ($this->isMasterUser($request)) {

			$validator = $this->getValidator($request);

			$user = \App\User::find($id);
			$user->name = 'NEW NAME!';
			$user->save();
			return UserTransformer::transform($user);
		} else {
			return response()->json(['error' => 'unauthorized', 401]);
		}	
	}

    /**
     * REST store
     * @api
     * @param Request $request
     * @return Response
     */
	public function store(Request $request)
	{
		if ($this->isMasterUser($request)) {

			$validator = $this->getValidator($request);

			$user = $this->userFactory->createByRoleName(
				$request->input('name'),
				$request->input('password'),
				$request->input('role'),
				$request->input('email')
			);

			return UserTransformer::transform($user);
		} else {
			return response()->json(['error' => 'unauthorized', 401]);
		}	
    }
    
    /**
     * REST destroy
     * @api
     * @param Request $request
     * @param [type] $id
     * @return void
     */
	public function destroy(Request $request, $id)
	{
		if ($this->isMasterUser($request)) {
			try {
				return $this->userRepository->delete($id);
			} catch (ModelNotFoundException $e) {
				return response()->json(['error' => 'not_found', 404]);
			}
	    } else {
			return response()->json(['error' => 'unauthorized', 401]);
		}	
	}


}