<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller as BaseController;

/**
 * A base controller for the API (that could be abstract). Although 
 * no real implementation is made here, any common logic between API
 * endpoint controllers could/should be placed here.
 */
class Controller extends BaseController
{


}