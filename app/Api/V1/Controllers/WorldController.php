<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

use App\Repositories\World\ThingRepository;
use App\Services\CharacterService;
use App\Services\UserService;

use App\Api\V1\Transformers\ThingTransformer;

/**
 * The World endpoints represent interactions with the surrounding world
 */
class WorldController
{

    /**
     * Thing repository
     * @var ThingRepository
     */
    private $thingRepository;

    /**
     * Character service
     * @var [type]
     */
    private $characterService;

    /**
     * UserService
     * @var UserService
     */
    private $userService;

    /**
     * Constructor
     * @param ThingRepository $thingRepository
     * @param UserService $userService
     * @param CharacterService $characterService
     */
    public function __construct(
        ThingRepository $thingRepository,
        UserService $userService,
        CharacterService $characterService)
    {
        $this->thingRepository = $thingRepository;
        $this->userService = $userService;
        $this->characterService = $characterService;
    }

    /**
     * Look around and return object withing the character's
     * gaze range
     * @api
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function look(Request $request)
    {

        $user = $request->user();
        $character = $this->userService->getCharacter($user);

        // How far can this character look
        $gazeRange = $character->getGaze();

        // Get the position
        $position = $character->getPosition();

        // Get all what is in range of the gaze
        $whatsAround = $this->thingRepository
                            ->getInSquareRange($position, $gazeRange)
                            ->get();

        // Output
        return ThingTransformer::transformCollection($whatsAround);

    }

}