<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Services\AuthService;
use App\Exceptions\UnauthorizedException;
use App\Exceptions\MissingCredentialsException;

use Auth;
use JWTAuth;

/**
 * Login controller
 */
class LoginController
{

	/**
	 * Login endpoint. Accepts "user" and "password" parameters
     * as a payload and builds a response with the JWT token if the
     * login attempt is successful. The token can be given as a 
     * plain text response with a "plaintext" parameter in the payload.
     * 
     * Throws error response if not valid.
     * @api
	 * @param  Request $request [description]
	 * @return Response       
	 */
	public function login(Request $request, AuthService $authService)
	{
		$user = $request->input('user');
		$password = $request->input('password');
		$plaintext = !!$request->input('plaintext');

		try {
            $token = $authService->loginAttempt($user, $password);
		} catch (UnauthorizedException $e) {
			return response()->json(['error' => 'unauthorized'], 401);
		} catch (MissingCredentialsException $e) {
			return response()->json(['error' => 'missing_credentials'], 401);
		} catch (JWTException $e) {
			return response()->json(['error' => 'cant_create_token'], 401);
		} catch (\Exception $e) {
			return response()->json(['error' => $e->getMessage(), 500]);
		}
		return $plaintext ? $token : compact('token');

	}

    /**
     * Logout operation, invalidates the token
     * @api
     * @param Request $request
     * @return void
     */
	public function logout(Request $request)
	{
		$token = JWTAuth::getToken();
		JWTAuth::invalidate($token);
	}

}