<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

use App\Models\World\Position;

use App\Services\UserService;
use App\Services\CharacterService;
use App\Services\FightService;

use App\Exceptions\OutOfBoundsException;
use App\Exceptions\TileOccupiedException;
use App\Exceptions\OutOfRangeException;
use App\Exceptions\NotAnEnemyException;

use App\Api\V1\Transformers\ThingTransformer;
use App\Api\V1\Transformers\ThingInventoryTransformer;

/**
 * Controller for the Character (which is the playing entity associated
 * with the User)
 * @see App\Models\Things\Character
 */
class CharacterController extends Controller
{

	/**
	 * User Service
	 * @var UserService
	 */
	private $userService;

	/**
	 * Fight Service
	 * @var FightService
	 */
	private $fightService;

    /**
     * Constructor
     * @param UserService $userService
     * @param FightService $fightService
     */
	public function __construct(UserService $userService, FightService $fightService)
	{
		$this->userService = $userService;
		$this->fightService = $fightService;
	}

	/**
	 * Pick objects
	 * @api
	 * @param  Request          $request          [description]
	 * @param  CharacterService $characterService [description]
	 * @return [type]                             [description]
	 */
	public function pick(Request $request, CharacterService $characterService)
	{
		$user = $request->user();
		$character = $this->userService->getCharacter($user);
		$inventory = $characterService->pick($character);
		return ThingInventoryTransformer::transformCollection($inventory);
	}
	
	/**
	 * Move character.
	 * Accepts "direction" and "pace" as POST parameters.
	 * @api
	 * @param  Request          $request          [description]
	 * @param  CharacterService $characterService [description]
	 * @return [type]                             [description]
	 */
	public function move(Request $request, CharacterService $characterService)
	{

		$direction = $request->input('direction');
		$pace = $request->input('pace', 1);

		if (!$direction) {
			return response()->json(['error' => 'bad_request'], 400);
		}

		$user = $request->user();
		$character = $this->userService->getCharacter($user);

		try {
			$whatIsOnTarget = $characterService->move($character, $direction, $pace);
		} catch (OutOfBoundsException $e) {
			return response()->json(['error' => 'out_of_bounds'], 401);
		} catch (TileOccupiedException $e) {
			return response()->json([
				'error' => 'tile_occupied',
				'what' => ThingTransformer::transformCollection($e->getOccupiedByWhat())
			], 401);
		}

		return response()->json([
			'message' => 'success',
			'current_position' => $character->getPosition(),
			'whats_here' => ThingTransformer::transformCollection($whatIsOnTarget)
		], 200);
		
    }
    
    /**
     * Attack a foe
     * @api
     * @param Request $request
     * @return void
     */
    public function attack(Request $request)
    {
        $user = $request->user();
        $character = $this->userService->getCharacter($user);
        
        $x = $request->input('x');
        $y = $request->input('y');
        $targetPosition = new Position($x, $y);

        try {
	        $this->fightService->attack($character, $targetPosition);
	    } catch (OutOfRangeException $e) {
	    	return response()->json(['error' => 'out_of_range'], 401);
	    } catch (NotAnEnemyException $e) {
	    	return response()->json(['error' => 'not_an_enemy'], 401);
	    }

	    return response()->json(['message' => 'ok'], 200);
        
    }

}