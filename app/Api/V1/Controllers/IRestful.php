<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

/**
 * This interface represents a base contract for the
 * main REST operations.
 */
interface IRestful
{

	public function index(Request $request);

	public function show(Request $request, $id);

	public function update(Request $request, $id);

	public function store(Request $request);

	public function destroy(Request $request, $id);

}