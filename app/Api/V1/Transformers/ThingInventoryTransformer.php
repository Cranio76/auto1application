<?php

namespace App\Api\V1\Transformers;

class ThingInventoryTransformer extends AbstractTransformer
{

	public static function transform($thing = null)
	{
		if (!$thing) {
			return null;
		}
		return [
			'id' => $thing->getId(),
			'what' => $thing->getWhat(),
			'type' => $thing->getType()
		];

	}

}