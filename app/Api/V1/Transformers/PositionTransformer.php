<?php

namespace App\Api\V1\Transformers;

use App\Models\World\Position;

/**
 * Transforms the Position
 */
class PositionTransformer extends AbstractTransformer
{

    /**
     * Transform
     * @param Position $position
     * @return Array
     */
	static function transform($position)
	{
		return $position->getCoords();
	}

}