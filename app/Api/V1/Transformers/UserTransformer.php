<?php

namespace App\Api\V1\Transformers;

use App\User;

class UserTransformer extends AbstractTransformer
{

	public static function transform($user)
	{
		return [
			'name' => $user->getName(),
			'character' => UserCharacterTransformer::transform($user->getCharacter()),
			'role' => $user->getRoleName()
		];
	}

}