<?php

namespace App\Api\V1\Transformers;

use App\Models\Things\Character;

use ThingInventoryTrasnformer;

class UserCharacterTransformer
{

	static function transform($character=null)
	{

		if (!$character) {
			return null;
		}
		return [
			'name' => $character->getName(),
			'position' => PositionTransformer::transform($character->getPosition()),
			'stamina' => $character->getStamina(),
			'attack' => $character->getAttack(),
			'defense' => $character->getDefense(),
			'inventory' => ThingInventoryTransformer::transformCollection($character->getInventory()),
			'gaze' => $character->getGaze(),
			'pace' => $character->getPace()
		];
	}

}