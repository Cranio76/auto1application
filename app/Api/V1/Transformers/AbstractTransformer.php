<?php

namespace App\Api\V1\Transformers;

/**
 * This abstract transformer provides the basic functionality
 * to transform-serialize any kind of data for the proper
 * output.
 * 
 * These Transformers allow us to:
 * - output data with the API in the format that suits us best
 * - output meaningful JSON objects with a structure
 * - output the same object in diverse fashions depending on what we need
 *   (see ThingTransformer and ThingInventoryTrasnformer)
 * - hide sensitive info from he objects
 */
abstract class AbstractTransformer
{

    /**
     * Transform a single item
     * @param Mixed $something
     * @return void
     */
	abstract public static function transform($something);

	/**
	 * Transforms a collection using the specified transform method
	 * @param  Mixed $collection 
	 * @return collection
	 */
	public static function transformCollection($collection)
	{

		if (!$collection instanceof \Illuminate\Support\Collection) {
			$collection = collect($collection);
		}

		return $collection->map(function ($item) {
			return static::transform($item);
		});

	}

	// @TODO
	// public function paginate($pageSize, $pageNumber)

}