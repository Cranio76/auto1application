<?php

namespace App\Factories;

use App\Models\World\Position;
use App\Models\Things\Character;
use Validator;

class CharacterFactory
{

    /**
     * Create a character, possibly associating it with a player (User)
     * @param  string   $name     
     * @param  Position $position The starting position
     * @param  int      $stamina  The strength
     * @param  int      $attack   Attack ratio
     * @param  int      $defense  Defense ratio
     * @param  int|null $userId   Player Id
     * @return Character             
     */
    public function create(string $name, Position $position, int $stamina, int $attack, int $defense, int $userId=null)
    {

        $character = new Character;
        $character->name = $name;
        $character->setPosition($position);
        $character->stamina = $stamina;
        $character->attack = $attack;
        $character->defense = $defense;
        $character->user_id = $userId;
        $character->gaze = 1;

        $character->save();
        return $character;
    }

    /**
     * Create a character with standard values
     * @param  string   $name   
     * @param  int|null $userId 
     * @return Character           
     */
    public function createStandardPlayer(string $name, int $userId=null)
    {
        return $this->create($name, new Position(0, 0), 500, 100, 100, $userId);
    }

}