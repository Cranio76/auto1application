<?php

namespace App\Factories;

use App\User;
use App\Models\Role;

class UserFactory
{

	/**
	 * Create a new User providing its role as a string key
	 * @param  string $name    
	 * @param  string $password
	 * @param  string $roleName
	 * @param  string $email   
	 * @return \App\User         
	 */
	public function createByRoleName(
		string $name,
		string $password,
		string $roleName,
		string $email)
	{

		$user = new User;
		$user->name = $name;
		$user->password = bcrypt($password);
		$user->role_id = Role::where('name', $roleName)->first()->id;
		$user->email = $email;
        $user->save();
        
        return $user;

	}

}