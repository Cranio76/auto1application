<?php

namespace App\Factories;

use App\Models\World\Thing;

/**
 * This factory does Things. Literally.
 */
class ThingFactory
{

	/**
	 * Create a world Thing in a position
	 * @param  int    $x    x coordinate
	 * @param  int    $y    y coordinate
	 * @param  string $what type of Thing
	 * @param  string $type subtype o Thing
	 * @return Thing
	 */
	public function create(int $x, int $y, string $what, $type='')
	{
		$thing = new Thing;
		$thing->x = $x;
		$thing->y = $y;
		$thing->what = $what;
		$thing->type = $type;
		$thing->save();

		return $thing;
	}

}