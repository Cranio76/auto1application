<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Services\UserService;

/**
 * This middleware checks if an User has an actual character
 * associated
 */
class MustHaveCharacterCreated
{

    /**
     * The service
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle middleware
     * @param  [type]  $request [description]
     * @param  Closure $next    [description]
     * @param  [type]  $guard   [description]
     * @return [type]           [description]
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
        return $this->userService->getCurrentCharacter() 
             ? $next($request) 
             : response()->json(['error' => 'no_character', 401]);
    }

}