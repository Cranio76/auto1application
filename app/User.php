<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Models\Things\Character;
use App\Models\Role;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * Eloquent relation with character
     * @return HasOne 
     */
    public function character()
    {
        return $this->hasOne(Character::class);
    }

    /**
     * Eloquent relation with Role
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    // GETTERS AND SETTERS
    
    /**
     * id getter
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * name getter
     * @return [type] [description]
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Character relation
     * @return \App\Models\Things\Character
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * Role
     * @return string
     */
    public function getRoleName()
    {
        return $this->role->name;
    }
}
