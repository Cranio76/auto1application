<?php

namespace App\Repositories\World;

use App\Models\World\World;
use App\Models\World\Thing;
use App\Models\World\Position;
use App\Repositories\IRepository;

/**
 * Repository for the Things
 */
class ThingRepository implements IRepository
{

    /**
     * Get what's in a specific position
     * @param  Position $position [description]
     * @return QueryBuilder 
     */
    public function getFromPosition(Position $position)
    {
        return Thing::where('x', $position->x)
            ->where('y', $position->y);
    }

    /**
     * Gets all the things in a bounding box
     * @param  Position $a A corner of the bounding box
     * @param  Position $b Another corner of the bounding box
     * @return Mixed       Things
     */
    public function getInBoundingBox(Position $a, Position $b)
    {
        $worldSize = World::getWorldSize();

        $x1 = max(min($a->x, $b->x), 0);
        $x2 = min(max($a->x, $b->x), $worldSize->x);
        
        $y1 = max(min($a->y, $b->y), 0);
        $y2 = min(max($a->y, $b->y), $worldSize->y);

        return Thing::whereBetween('x', [$x1, $x2])
            ->whereBetween('y', [$y1, $y2]);
    }

    /**
     * Gets all the things in a square range from a center  
     * @param  Position $position Center
     * @param  int      $range    Range in tiles
     * @return Mixed              Things
     */
    public function getInSquareRange(Position $position, int $range)
    {
        $corner = new Position($range, $range);

        return $this->getInBoundingBox(
            $position->sub($corner),
            $position->add($corner)
        );
    }

    /**
     * Gets all the things in a circular range from a center    
     * @param  Position $position Center
     * @param  int      $radius   Radius in tiles
     * @return Mixed              Things
     */
    public function getInCircularRange(Position $position, int $radius)
    {
        // NOT IMPLEMENTED
    }

    /**
     * Get things we can't tread on (obstacles like walls, foes)
     * @param  Position $position [description]
     * @return Collection             
     */
    public function getUntreadableInPosition(Position $position)
    {
        return $this->getFromPosition($position)
                    ->untreadable()
                    ->get();
    }

    /**
     * Get all things in a position as collection
     * @param  Position $position 
     * @return Collection             
     */
    public function getAllInPosition(Position $position)
    {
        return $this->getFromPosition($position)
                    ->get();
    }

    /**
     * Get all things we can pick in a position
     * @param  Position $position 
     * @return Collection             
     */
    public function getPickableInPosition(Position $position)
    {
        return $this->getFromPosition($position)
                    ->pickable()
                    ->get();
    }


    
}