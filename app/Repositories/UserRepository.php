<?php

namespace App\Repositories;

use App\Repositories\Contracts\IUserRepository;
use App\User;

/**
 * Repository for Users. For time reasons some necessary checks
 * are not implemented. 
 * In real world scenarios, I would check the actual existence
 * of the object and throw a ModelNotFound exception. 
 * The calling controller would intercept it and give a 404
 * response with a proper error message.
 */
class UserRepository implements IRepository, IUserRepository
{

    public function find($id)
    {
        return User::find($id);
    }

    public function index()
    {
        return User::all();
    }

    public function create($name, $password, $role, $email)
    {
        $factory = new \App\Factories\UserFactory;
        $factory->createByRoleName($name, $password, $role, $email);
    }

    public function delete($id)
    {
        User::destroy($id);
    }

    public function update($id, $data)
    {
        $user = $this->find($id);

        if (isset($data['role']))
        {
            $role_id = Role::where('name', $roleName)->first()->id;
            $user->role_id = $role_id;
        }

        if ($user)
        {
            $user->name = $data['name'] ?? $user->name;
            $user->password = $data['password'] ?? $user->password;
            $user->email = $data['email'] ?? $user->email;
        }

        $user->save();
    }
    
}