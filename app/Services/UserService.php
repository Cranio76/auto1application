<?php

namespace App\Services;

use Auth;
use App\Models\Things\Character;

class UserService
{

	public function getCurrentUser()
	{
		return Auth::user();
	}

	/**
	 * [getCurrentCharacter description]
	 * @return Character 
	 */
	public function getCurrentCharacter()
	{
		return Auth::user()->character;
	}

	public function getCharacter(\App\User $user)
	{
		return $user->character;
	}

}