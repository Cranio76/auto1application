<?php

namespace App\Services;

use App\Services\UserService;
use App\Services\CharacterService;

use App\Models\Things\Character;
use App\Models\World\Thing;
use App\Models\World\World;

use Illuminate\Support\Facades\Storage;

use App\User;

class GameService
{

	/**
	 * [$userService description]
	 * @var UserService
	 */
	private $userService;

	/**
	 * [$characterService description]
	 * @var CharacterService
	 */
	private $characterService;

	public function __construct(UserService $user, CharacterService $character)
	{
		$this->userService = $user;
		$this->characterService = $character;
	}

	public function load()
	{
		return unserialize(Storage::get('games/savegame'));
	}

	/**
	 * Saves the game. 
	 * @return [type] [description]
	 */
	public function save(User $user)
	{
		$game = [];
		$game['version'] = 1;
		$game['user'] = $user;
		$game['world'] = [
			'size' => World::getWorldSize()
		];
		$game['characters'] = Character::all();
		$game['things'] = Thing::all();

		Storage::put('games/savegame', serialize($game));

		return $game;

	}

}