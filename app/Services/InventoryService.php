<?php

namespace App\Services;

use App\Models\Things\Character;
use App\Models\World\Thing;
use Illuminate\Support\Facades\Cache;

class InventoryService
{

    /**
     * Gets the characters inventory
     * @param  Character $character 
     * @return Array               Array of things
     */
    public function getInventory(Character $character)
    {
        return Cache::get('INVENTORY_' . $character->getId(), []);
    }

    /**
     * Sets the character's inventory
     * @param Character $character 
     * @param Array    $inventory Array of things
     */
    public function setInventory(Character $character, $inventory)
    {
        $key = 'INVENTORY_' . $character->getId();
        Cache::forever($key, $inventory);
    }    

    /**
     * Adds to the characters inventory
     * @param Character $character [description]
     * @param Thing     $thing     [description]
     */
    public function addToInventory(Character $character, Thing $thing)
    {
        $thing->removeFromWorld();
        $thing->save();

        $inventory = $this->getInventory($character);
        $inventory[] = $thing;
        $this->setInventory($character, $inventory);
        return $inventory;
    }


}