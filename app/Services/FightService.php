<?php

namespace App\Services;

use App\Models\Things\Character;
use App\Repositories\World\ThingRepository;
use App\Models\World\Position;
use App\Exceptions\OutOfRangeException;
use App\Exceptions\NotAnEnemyException;

class FightService
{

	/**
	 * Thing Repository
	 * @var ThingRepository
	 */
	private $thingRepository;

	public function __construct(ThingRepository $thingRepository)
	{
		$this->thingRepository = $thingRepository;
	}
    
    /**
     * Attack a position
     * @param  Character $character      [description]
     * @param  Position  $targetPosition [description]
     * @return [type]                    [description]
     */
    public function attack(Character $character, Position $targetPosition)
    {
    	$position = $character->getPosition();

    	if (!$position->isInSquareRangeWith($targetPosition, 1))
    	{
    		throw new OutOfRangeException;
    	}

		$whatsThere = $this->thingRepository->getFromPosition($targetPosition)
			->where('what', 'foe')
			->first();

		if ($whatsThere) {
			$whatsThere->setPosition(new Position(-1, -1));
			$whatsThere->save();
		} else {
			throw new NotAnEnemyException;
		}

    	

    }

}