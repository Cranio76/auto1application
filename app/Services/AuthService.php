<?php

namespace App\Services;

use Auth;
use JWTAuth;
use App\Exceptions\MissingCredentialsException;
use App\Exceptions\UnauthorizedException;
/**
 * Custom Auth service. In out implementation, it Useful as a bridge between the backend
 * auth mechanisms of Laravel (provided through the `Auth` façade)
 * and the JWT based 
 */
class AuthService
{

	public function loginAttempt($name, $password)
	{

		if (!$name || !$password) {
			throw new MissingCredentialsException;
		}

        $credentials = compact('name', 'password');
        
		if (Auth::attempt($credentials)) {
			if (!$token = JWTAuth::attempt($credentials)) {
				throw new UnauthorizedException;
            }
			return $token;
		} else {
			throw new UnauthorizedException;
		}

	}

}