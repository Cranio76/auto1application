<?php

namespace App\Services;

use App\Models\Things\Character;
use App\Models\World\Position;
use App\Models\World\World;
use App\Models\World\Thing;
use App\Repositories\World\ThingRepository;

use App\Services\InventoryService;

use App\Exceptions\OutOfBoundsException;
use App\Exceptions\TileOccupiedException;

use Cache;

class CharacterService
{

    /**
     * [$thingRepository description]
     * @var ThingRepository
     */
    private $thingRepository;

    /**
     * [$inventoryService description]
     * @var InventoryService
     */
    private $inventoryService;

    public function __construct(ThingRepository $things, InventoryService $inventory)
    {
        $this->thingRepository = $things;
        $this->inventoryService = $inventory;
    }

    /**
     * Move the character in some direction
     * @param  Character|null $char      [description]
     * @param  string|null    $direction [description]
     * @param  int|integer    $pace      [description]
     * @return [type]                    [description]
     */
    public function move(Character $char=null, string $direction=null, int $pace=1)
    {

        $position = $char->getPosition();
        $targetPosition = $position->moveDirection($direction, $pace);

        if (World::isPositionOutOfBounds($targetPosition))
        {
            throw new OutOfBoundsException;
        }

        $whatIsUntreadableOnTarget = $this->thingRepository
            ->getUntreadableInPosition($targetPosition);
            
        if ($whatIsUntreadableOnTarget->count())
        {
            throw new TileOccupiedException($whatIsUntreadableOnTarget);
        }

        $whatIsOnTarget = $this->thingRepository
            ->getFromPosition($targetPosition)
            ->get();

        // Actually update the positino
        $char->setPosition($targetPosition);
        $char->save();

        return $whatIsOnTarget;
    }


    /**
     * Picks object in position
     * @return [type] [description]
     */
    public function pick(Character $character)
    {
        $whatIsPickable = $this->thingRepository
            ->getPickableInPosition($character->getPosition());

        foreach($whatIsPickable as $item)
        {
            $this->inventoryService->addToInventory($character, $item);
        }

        return $this->inventoryService->getInventory($character);
    }

    /**
     * Looks around
     * @param  Character $character 
     * @return Thing               
     */
    public function look(Character $character)
    {
        return $this->thingRepository
            ->getInSquareRange($character->getPosition(), $character->getGaze())
            ->get();
    }
}