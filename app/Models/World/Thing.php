<?php

namespace App\Models\World;

use Illuminate\Database\Eloquent\Model;

use App\Models\World\Position;
use App\Models\World\PositionTrait;

class Thing extends Model
{
    
	use PositionTrait;

	public function getId()
	{
		return $this->id;
	}

	public function getWhat()
	{
		return $this->what;
	}

	public function getType()
	{
		return $this->type;
	}

	/**
	 * This is a Laravel scope to limit the query to the untreadable
	 * objects.
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeUntreadable($query)
	{
		return $query->where('what', '!=', 'weapon');
	}


	/**
	 * This is a Laravel scope to limit the query to objects
	 * we can pick
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopePickable($query)
	{
		return $query->where('what', 'weapon');
	}
		
    
}
