<?php

namespace App\Models\World;

/**
 * This class contains some useful methods regarding
 * the world of Localhostland as a whole.
 */
class World
{

	/**
	 * Obtains the world's size as a Position vector of
	 * [size-1, size-1] coordinates
	 * @return Position World size
	 */
	static public function getWorldSize() : Position
	{
		return new Position(3, 3);
	}

	/**
	 * Verify if a position is out of Localhostland
	 * @param  Position $position 
	 * @return boolean            
	 */
	static public function isPositionOutOfBounds(Position $position)
	{
		$worldSize = static::getWorldSize();

		return $position->x < 0
			|| $position->y < 0
			|| $position->x > $worldSize->x
			|| $position->y > $worldSize->y;

	}

}