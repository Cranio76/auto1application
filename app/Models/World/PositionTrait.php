<?php

namespace App\Models\World;

trait PositionTrait
{
	/**
	 * [setPosition description]
	 * @param Position $position [description]
	 */
	public function setPosition(Position $position)
	{
		$this->x = $position->x;
		$this->y = $position->y;
	}

	/**
	 * [getPosition description]
	 * @return [type] [description]
	 */
	public function getPosition() : Position
	{
		return new Position($this->x, $this->y);
	}

	public function removeFromWorld()
	{
		$this->setPosition(new Position(-1, -1));
	}
}