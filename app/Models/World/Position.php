<?php

namespace App\Models\World;

/**
 * Represents a position (i.e. a point or a vector) in 
 * the World of LocalhostLand
 */
class Position
{

	/**
	 * Guess what, the x coordinate
	 * @var int
	 */
	public $x;

	/**
	 * Gues what, they coordinate
	 * @var int
	 */
	public $y;

	/**
	 * Constructor		
	 * @param int $x x coordinate
	 * @param int $y y coordinate
	 */
	public function __construct(int $x, int $y)
	{
		$this->x = $x;
		$this->y = $y;
	}

	/**
	 * Add two vectors together
	 * @param Position $pos 
	 * @return Position
	 */
	public function add(Position $pos) : Position
	{
		return new Position($pos->x + $this->x, $pos->y + $this->y);
	}

	/**
	 * Negates a vector
	 * @return Position 
	 */
	public function neg() : Position
	{
		return new Position(-$this->x, -$this->y);
	}

	/**
	 * Subtracts a vector from another
	 * @param  Position $pos 
	 * @return Position        
	 */
	public function sub(Position $pos) : Position
	{
		return $this->add($pos->neg());
	}

	/**
	 * Gets the vector's modulo
	 * @return float
	 */
	public function modulo() : float
	{
		return sqrt($this->x * $this->x + $this->y * $this->y);
	}

	/**
	 * Calculates the Pythagorean distance between two vectors/points
	 * @param  Position $pos 
	 * @return float        
	 */
	public function distance(Position $pos) : float
	{
		return $this->sub($pos)->modulo();
	}

	/**
	 * Get the coordinates as Array
	 * @return Array 
	 */
	public function getCoords() : Array
	{
		return [ $this->x, $this->y ];
    }
    
    /**
     * Obtain new position by moving in a specified direction
     * given by string ('e', 'nw', ..)
     * @param string $direction
     * @return void
     */
    public function moveDirection(string $direction, int $distance = 1)
    {
        $xVector = new Position($distance, 0);
        $yVector = new Position(0, $distance);

        switch($direction) {
            case 'n':
                $targetPosition = $this->sub($yVector);
                break;
            case 's':
                $targetPosition = $this->add($yVector);
                break;
            case 'e':
                $targetPosition = $this->add($xVector);
                break;
            case 'w':
                $targetPosition = $this->sub($xVector);
                break;
            case 'ne':
                $targetPosition = $this->add($xVector)->sub($yVector);
                break;
            case 'nw':
                $targetPosition = $this->sub($xVector)->sub($yVector);
                break;
            case 'se':
                $targetPosition = $this->add($xVector)->add($yVector);
                break;
            case 'sw':
                $targetPosition = $this->sub($xVector)->add($yVector);
                break;
        }

        return $targetPosition;
    }

    /**
     * Returns true if a position is within "square range" with the current.
     * Square range is calculated taking the maximum absolute difference
     * between coordinates.
     * @param  Position $position [description]
     * @param  int      $range    [description]
     * @return boolean            [description]
     */
    public function isInSquareRangeWith(Position $position, int $range)
    {
    	$delta = $this->sub($position);
    	$ax = abs($delta->x);
    	$ay = abs($delta->y);
    	$min = max($ax, $ay);
    	return $min <= $range;
    }

    public function __toString(){
    	return '[' + $this->x + ',' + $this->y + ']';
    }

}