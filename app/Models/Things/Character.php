<?php

namespace App\Models\Things;

use Illuminate\Database\Eloquent\Model;

use App\Models\World\Position;
use App\Models\World\PositionTrait;

use App\Services\InventoryService;

class Character extends Model
{
    
    use PositionTrait;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
    	return $this->name;
    }

    public function getStamina()
    {
    	return $this->stamina;
    }

    public function getAttack()
    {
    	return $this->attack;
    }

    public function getDefense()
    {
    	return $this->defense;
    }
    
    public function getInventory()
    {
        $inventory = new InventoryService;
        return $inventory->getInventory($this);
    }

    public function getGaze()
    {
        return $this->gaze;
    }

    public function getPace()
    {
        return $this->pace;
    }
}
