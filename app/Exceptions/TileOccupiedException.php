<?php

namespace App\Exceptions;

use Exception;

class TileOccupiedException extends Exception
{

	private $occupiedByWhat;

	public function __construct($occupiedByWhat)
	{
		$this->occupiedByWhat = $occupiedByWhat;
	}

	public function getOccupiedByWhat()
	{
		return $this->occupiedByWhat;
	}

}