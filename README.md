# LORD OF THE PINGS - RPG Assignment

**Hello hiring folks!**

I decided to use **Laravel 5.5** as a base skeleton to code the challenge. Why?
Because it's a beautiful framework, and I have a lot of confidence and 
familiarity with it. Hope you'll like it!

This `README.md` has been carefully crafted with **markdown formatting** and 
proper document hierarchy. 

As a creative type of guy, I value documentation aesthetics a lot so it should
be nice to read :)

An **automatic HTML rendering of this docs** with links can be found under the
`/docs/auto` folder. Use `README.html` as starting point.

## Which files to look at?

Some files are part of the framework. The custom parts made by me are: 

* all the `/app/Api`,
* all the `/app/Exceptions` folder
* all the `/app/Factories` folder
* the `/app/Http/Middleware/CustomApiAuth.php` and 
  `/app/Http/Middleware/MustHaveCharacterCreated.php` files
* `/app/User.php` file 
* the `/app/Models` folder, 
* the `/app/Repositories` folder
* the `/app/Services` folder
* migrations and seeds in `/database`
* `docker-compose.yml` and `/docker` folder
* `/docs` folder and `README.md`
* `/resources/assets/js` front-end (bundled in `app.js` under `/public/js`)
* `/routes`
* `/tests`
* `.env`, `.env.testing`, `phpunit.xml`, `phpdoc.dist.xml`, `gulpfile.js` and
  a bunch of other files here and there

## Subdocs

### Installation & running

All the useful info about **requirements**, my own Docker 
containerization with solutions for testing, other install 
procedures etc. See `/docs/install.md`.

### Coding style and code architecture

Wanna know how I conceived the whole thing, what practices I adhere to, patterns 
I've used? See `/docs/codingstyle.md`.

### Documentation

This project features **PHPDoc documentation and generation**, **Markdown
docs** and a simple **Gulp pipeline to convert markdown to HTML docs**.
See `/docs/docs.md`.

### Data architecture

How are things 'n stuff organized in *Lord of the Pings*? See `/docs/data.md`

### Game architecture

The key concepts beneath the game. See `/docs/game.md`

### Class documentation

A convenient PHPDoc-generated **class documentation** is under `/docs/html`.

### API

The very core of the game, that will hopefully get me the job. Er, the job 
*done* (cough). See `/docs/api.md`

### Walkthrough

So, all is set, how about actually playing the code? I bow to
your love of CLIs: and therefore there is a 
**complete copy-pastable walkthrough**. See `/docs/walkthrough.md`

### Frontend

Some info also about the **frontend implementation**. See `/docs/frontend.md`.

### Testing

All about the **test part**, with all the implementation and coding philosophy
behind the tests. See `/docs/testing.md`

### Security

Some pointers in `/docs/security.md`

### Scalability and Extensibility

Some consideratons in `/docs/scalability.md`